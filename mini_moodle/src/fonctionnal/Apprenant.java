package fonctionnal;

public class Apprenant extends Personne{
	private int nb_reponse_correcte;
	
	public Apprenant(String nom, String prenom, String identifiant, String mot_de_passe) {
		super(nom, prenom, identifiant, mot_de_passe);
		this.nb_reponse_correcte = 0;
	}

	public int getNb_reponse_correcte() {
		return nb_reponse_correcte;
	}

	public void setNb_reponse_correcte(int nb_reponse_correcte) {
		this.nb_reponse_correcte = nb_reponse_correcte;
	}
	
	public String toString() {
		return "Professeur [nom = " + super.getNom() + ", prenom = " + super.getPrenom() + ", identifiant = " + super.getIdentifiant() + 
				", mot_de_passe = " + super.getMot_de_passe() + "]";
	}

	public boolean equals(Apprenant student) {
		if (student == null) {
			return false;
		} else if (student.getIdentifiant() == this.getIdentifiant()) {
			return true;
		} else {
			return false;
		}
	}
}
