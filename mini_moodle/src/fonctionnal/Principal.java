package fonctionnal;

import java.io.*;

public class Principal {
	static public void main(String args[]) {
		
		File file = new File(lienFic);
		
		if (!file.exists()) {
			try {
				file.createNewFile();
			}
			catch (IOException e) {
				e.printStackTrace();
			}
		}
		Professeur gwendal = new Professeur("Le Bouffant", "Gwendal", "glebouff", "mdpgwendal", "Analyse");
		System.out.println("|*******************|\n|* Creation Classe *|\n|*******************|\n" + gwendal + "\n");
		Classe klaas = new Classe(gwendal);
		System.out.println(klaas + "\n\n|**********************|\n|* Remplissage Classe *|\n|**********************|");
		Apprenant eliot = new Apprenant("le joncour","eliot", "lejoncei", "blabla");
		Apprenant gweltaz = new Apprenant("Peinte", "Gweltaz", "peintegi", "tructruc");
		klaas.addStudent(eliot);
		klaas.addStudent(gweltaz);
		System.out.println(klaas + "\n\n|*****************|\n|* Edit Personne *|\n|*****************|");
		eliot.setIdentifants("lejonc", "blablou");
		eliot.setEtatCivil("Le Joncour", "Eliot");
		System.out.println(klaas + "\n");

		

		Ouverte q1 = new Ouverte("--- Ouverte ?? (Ouv)", "Q1003",3);
		QCM q2 = new QCM("--- Suis je un QCM ? (QCM)", "Q1004",10);
		
		
		Proposition prop5 = new Proposition("1) Oui une ouverture !", true);
		
		PropositionQCM prop6 = new PropositionQCM("1) Je suis un QCM ? (QCM)", true, 50);
		PropositionQCM prop7 = new PropositionQCM("2) Je suis un QCU particulier ? (QCM)", true, 50);
		PropositionQCM prop8 = new PropositionQCM("3) Je suis une rponse ? (QCM)", false, 0);
	
		q1.ajouterProposition(prop5);
		
		q2.ajouterPropositionQCM(prop6);
		q2.ajouterPropositionQCM(prop7);
		q2.ajouterPropositionQCM(prop8);
				

		Tentative_QCU tqcm5 = new Tentative_QCU(q1, "1) Oui une ouverture !");
		
		Tentative_QCM tqcm6 = new Tentative_QCM(q2, "1) Je suis un QCM ? (QCM)");
		Tentative_QCM tqcm7 = new Tentative_QCM(q2, "3) Je suis une rponse ? (QCM)");

		
		ListeTentativeQ LTQ1 = new ListeTentativeQ(gweltaz, q1);
		LTQ1.ajouterTent(tqcm5);
		
		ListeTentativeQCM LTQ2 = new ListeTentativeQCM(gweltaz, q2);
		LTQ2.ajouterTent(tqcm6);
		LTQ2.ajouterTent(tqcm7);
		
		
		ListeTentativesA LTA1 = new ListeTentativesA(gweltaz);
		LTA1.ajouterTentQ(LTQ1);
		LTA1.ajouterTentQCM(LTQ2);
		
		
		System.out.println("\n QUEST1 : \n");
		
		Questionnaire QUEST1 = new Questionnaire(gwendal);
		QUEST1.ajouter_Question(q1);
		QUEST1.ajouter_QCM(q2);
		QUEST1.ajouter_ListeTentativeA(LTA1);
		
		
		System.out.println("\n QUEST1 A : \n");
		QUEST1.afficher_questionnaire_a();
		
		System.out.println("\n QUEST1 P : \n");
		QUEST1.afficher_questionnaire_p();
		
		System.out.println("\n QUEST1 T : \n");
		QUEST1.afficher_Tentatives_Classe();
		
		DATA data = new DATA (LTA1, klaas, QUEST1, eliot, gwendal);
		Seria.ecrire(data, file);
		
	}
}
