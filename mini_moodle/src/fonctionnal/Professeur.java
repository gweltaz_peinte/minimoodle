package fonctionnal;

public class Professeur extends Personne{
	private String matiere_enseignee;
	
	public Professeur(String nom, String prenom, String identifiant, String mot_de_passe, String matiere_enseignee) {
		super(nom, prenom, identifiant, mot_de_passe);
		this.matiere_enseignee = matiere_enseignee;
	}

	public String getMatiere_enseignee() {
		return matiere_enseignee;
	}

	public void setMatiere_enseignee(String matiere_enseignee) {
		this.matiere_enseignee = matiere_enseignee;
	}
	
	public void setProf (String identifiant, String mot_de_passe, String matiere_enseignee) {
		super.setIdentifiant(identifiant);
		super.setMot_de_passe(mot_de_passe);
		this.setMatiere_enseignee(matiere_enseignee);
	}

	
	public String toString() {
		return "Prof de " + matiere_enseignee + " : " + this.getNom() + ", " + this.getPrenom();
	}
	
	public boolean equals(Professeur enseignant) {
		if (enseignant == null) {
			return false;
		} else if (enseignant.getIdentifiant() == this.getIdentifiant()) {
			return true;
		} else {
			return false;
		}
	}
}
