package fonctionnal;

import java.util.ArrayList;


public class PersonneManager {
	final int NOT_FOUND = -1;
	final int INCORRECT_MDP = -2;
	
	ArrayList<Personne> personnes;
	Personne connecte;
	
	public PersonneManager () {
		this.personnes = new ArrayList<Personne>();
		this.connecte = null;
	}
	
	//Est-ce que la personne existe ?
	public int exists(String identifiant, String mot_de_passe) {
		boolean trouve = false;
		int i = 0;
		int res = NOT_FOUND;
		while (!trouve && i< this.personnes.size()) {
			if (this.personnes.get(i).isLoginEqual(identifiant)) {
				if (this.personnes.get(i).isPwdEqual(mot_de_passe)) {
					trouve=true;
					res = i;
				} else {
					res = INCORRECT_MDP;
					break;
				}
			} else {
				i++;
			}
		}
		return res;
	}
	
	public void connexion(String identifiant, String mot_de_passe) {
		int exists = this.exists(identifiant, mot_de_passe);
		if (exists == NOT_FOUND) {
			System.out.println("[INTERFACE] identifiant incorrect, r�essayes encore");
		} else if (exists == INCORRECT_MDP) {
			System.out.println("[INTERFACE] mot de passe incorrect, r�essayes encore");
		} else {
			this.connecte = this.personnes.get(exists);
			System.out.println("[LOG] " + identifiant + " connect�");
		}
	}
	public void inscription(String nom,	String prenom, String identifiant, String mot_de_passe, String matiere_enseignee) {
		int exists = this.exists(identifiant, mot_de_passe);
		if (exists == NOT_FOUND) {
			personnes.add(new Professeur(nom, prenom, identifiant, mot_de_passe, matiere_enseignee));
			System.out.println("[LOG] Le professeur " + prenom + " " + nom + " inscrit sous le pseudo : " + identifiant);
			this.connexion(identifiant,mot_de_passe);
		} else {
			System.out.println("[INTERFACE] cet identifiant est d�j� connu");
		}
	}
	public void inscription(String nom,	String prenom, String identifiant, String mot_de_passe) {
		int exists = this.exists(identifiant, mot_de_passe);
		if (exists == NOT_FOUND) {
			personnes.add(new Apprenant(nom, prenom, identifiant, mot_de_passe));
			System.out.println("[LOG] L'�tudiant " + prenom + " " + nom + " inscrit sous le pseudo : " + identifiant);
			this.connexion(identifiant,mot_de_passe);
		} else {
			System.out.println("[INTERFACE] cet identifiant est d�j� connu");
		}
	}
	public void desinscription() {
		if(this.connecte != null) {
			//TODO supprimer toutes les compositions
			this.personnes.remove(this.connecte);
		}
	}
	public void affichePersonnes() {
		for(int i=0 ; i < this.personnes.size() ; i++) {
			this.personnes.get(i).affiche();
		}
	}
}
