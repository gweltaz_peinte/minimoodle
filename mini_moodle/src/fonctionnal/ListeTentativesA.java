package fonctionnal;
import java.util.ArrayList;

public class ListeTentativesA {
	
	public final int NOT_FOUND = -1;
	
	private ArrayList<ListeTentativeQ> liste_q;
	private ArrayList<ListeTentativeQCM> liste_qcm;
	private Apprenant student;
	
	// Constructeur de la Classe ListeTentativesA =======================================================
	
	public ListeTentativesA(Apprenant student) {
		liste_q = new ArrayList<ListeTentativeQ>();
		liste_qcm = new ArrayList<ListeTentativeQCM>();
		this.student = student;
	}
	
	// GETTERS & SETTERS =======================================================
	
	// Getters 
	
	public ArrayList<ListeTentativeQ> getliste_q() {
		return liste_q;
	}
	
	public ArrayList<ListeTentativeQCM> getliste_qcm() {
		return liste_qcm;
	}
	
	public Apprenant getStudent() {
		return this.student;
	}
	
	// Setters

	public void setliste_q(ArrayList<ListeTentativeQ> liste_q) {
		this.liste_q = liste_q;
	}

	public void setliste_qcm(ArrayList<ListeTentativeQCM> liste_qcm) {
		this.liste_qcm = liste_qcm;
	}
	
	public void setStudent(Apprenant student) {
		this.student = student;
	}
	
	
	
	// M�thodes =======================================================
	
	// Getter Getsomme_A
	
	public float Getsomme_A() {
		int i;
		float somme = 0;
		for (i=0; i < this.liste_q.size(); i++) {
			somme = somme + this.liste_q.get(i).Getsomme_note_lr();
		}
		for (i=0; i < this.liste_qcm.size(); i++) {
			somme = somme + this.liste_qcm.get(i).Getsomme_note_lr();
		}
		return somme;
	}
	
	// Afficher ListeTentativesA
	
	public void afficher() {
		int i;
		for (i=0 ; i < this.liste_q.size(); i++) {
			this.liste_q.get(i).getQuestion().afficherIntitule_points();
			this.liste_q.get(i).afficher();
		}
		for (i=0 ; i < this.liste_qcm.size(); i++) {
			this.liste_qcm.get(i).getQuestion().afficherIntitule_points();
			this.liste_qcm.get(i).afficher();
		}
	}
	
	// Ajouter une ListeTentativeQCM
	
	public void ajouterTentQCM(ListeTentativeQCM lq) {
		int i = 0;
		boolean trouve = false;
		while(i<this.getliste_qcm().size() && trouve!=true) {						// Cherche si Tentative_QCM d�j� pr�sente
			if (this.getliste_qcm().get(i) == lq) {
				trouve = true;
			}
			else {
				i = i+1;
			}
		}
		if (trouve == false) {
			this.liste_qcm.add(lq);													// Pas trouve alors ajout
		}
	}
	
	// Ajouter une ListeTentativeQ
	
	public void ajouterTentQ(ListeTentativeQ lq) {
		int i = 0;
		boolean trouve = false;
		while(i<this.getliste_qcm().size() && trouve!=true) {						// Cherche si Tentative_QCM d�j� pr�sente
			if (this.getliste_q().get(i) == lq) {
				trouve = true;
			}
			else {
				i = i+1;
			}
		}
		if (trouve == false) {
			this.liste_q.add(lq);													// Pas trouve alors ajout
		}
	}
	
	// Retirer une ListeTentativeQCM
	
	public void retirerListeTentativeQCM(ListeTentativeQCM lq) {
		boolean trouve = false;
		int i = 0;
		while(i<this.getliste_qcm().size() && trouve!=true) {						// Cherche si ListeTentativeQCM pr�sente
			if (this.getliste_qcm().get(i) == lq) {									// Cas trouv�e
				trouve = true;
				this.liste_qcm.remove(lq);											// On retire
			}
			else {
				i = i+1;
			}
		}
	}

	// Retirer une ListeTentativeQ
	
	public void retirerListeTentativeQ(ListeTentativeQ lq) {
		boolean trouve = false;
		int i = 0;
		while(i<this.getliste_q().size() && trouve!=true) {						// Cherche si ListeTentativeQCM pr�sente
			if (this.getliste_q().get(i) == lq) {									// Cas trouv�e
				trouve = true;
				this.liste_q.remove(lq);										// On retire
			}
			else {
				i = i+1;
			}
		}
	}
}


