package fonctionnal;

public class Proposition {
	
	private String intitule;
	private float pourcentage;
	private boolean correction;
	
	// Constructeur de la Super Classe Proposition =======================================================
	
	public Proposition(String inti, boolean corr) {
		this.intitule = inti;
		this.correction = corr;
		if (corr == true && this.getClass().getName() == "fonctionnal.Proposition") {			// Cas Proposition Vraie
			this.pourcentage = 100;																	// Pourcentage = 100%
		}
		else if (corr == false && this.getClass().getName() == "fonctionnal.Proposition") {		// Cas Proposition Fausse
			this.pourcentage = 0;																	// Pourcentage = 0%
		}
	}

	
	// GETTERS & SETTERS =======================================================
	
	// Getters
	
	public String getIntitule() {
		return this.intitule;
	}
	
	public float getPourcentage() {
		return this.pourcentage;
	}
	
	public boolean getCorrection() {
		return this.correction;
	}

	// Setters
	
	public void setIntitule(String intitule) {
		this.intitule = intitule;
	}

	public void setPourcentage(float pourcentage) {
		this.pourcentage = pourcentage;
	}

	public void setCorrection(boolean correction) {
		this.correction = correction;
	}
	

	
	// M�thodes =======================================================
	
	// Afficher Version Professeur intitul� de la Proposition, sa correction et ses points
	
	public void afficher_p() {
		System.out.println(this.getIntitule() + " : " + this.getCorrection() + " (" + this.getPourcentage() + " %)");
	}
	
	// Afficher Version Apprenant intitul� de la Proposition
	
	public void afficher_a() {
		System.out.println(this.getIntitule());
	}
	
}
