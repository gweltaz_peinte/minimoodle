package fonctionnal;

import java.util.ArrayList;

public class Questionnaire {
	public final int NOT_FOUND = -1;
	
	private int id;
	private float Points_Questionnaire;
	private Professeur enseignant;
	private ArrayList<Question> liste_question;
	private ArrayList<QCM> liste_QCM;
	private ArrayList<ListeTentativesA> liste_rep_c;
	
	static int nbQuestionnaire = 0;
	
// Constructeur de Questionnaire
	public Questionnaire(Professeur enseignant) {
		this.id = Questionnaire.getNbQuestionnaire();
		this.enseignant = enseignant;
		this.liste_question = new ArrayList<Question>();
		this.liste_QCM = new ArrayList<QCM>();
		this.liste_rep_c = new ArrayList<ListeTentativesA>();
		Questionnaire.setNbQuestionnaire(Questionnaire.getNbQuestionnaire()+1);
	}
	
// GETTERS & SETTERS =======================================================
	
	static public int getNbQuestionnaire() {
		return Questionnaire.nbQuestionnaire;
	}
	static public void setNbQuestionnaire(int nbQuestionnaire) {
		Questionnaire.nbQuestionnaire = nbQuestionnaire;
	}
	
	public float getPoints_Questionnaire() {
		float somme = 0;
		try{
			for (int i = 0; i < this.getliste_question().size(); i = i+1) {
				somme = somme + this.getliste_question().get(i).getnb_point();
			}
			for (int i = 0; i < this.getliste_QCM().size(); i = i+1) {
				somme = somme + this.getliste_QCM().get(i).getnb_point();
			}
			this.Points_Questionnaire = somme;
		}
		catch (java.lang.IndexOutOfBoundsException e) {}
		
		return this.Points_Questionnaire;
	}
	public void setPoints_Questionnaire(float point) {
		this.Points_Questionnaire = point;
	}
	
	public int getId() {
		return this.id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	public ArrayList<Question> getliste_question() {
		return this.liste_question;
	}
	
	public ArrayList<QCM> getliste_QCM() {
		return this.liste_QCM;
	}
	
	public ArrayList<ListeTentativesA> getListe_rep_c() {
		return this.liste_rep_c;
	}
	public Professeur getEnseignant() {
		return this.enseignant;
	}

// Afficher Questionnaire
	
	public void afficher_questionnaire_p() {
		try{
			for (int i = 0 ; i < this.getliste_question().size() ; i++) {
				this.getliste_question().get(i).afficherQuestion_p();
				this.getliste_QCM().get(i).afficherQuestion_p();
			}
		}
		catch (java.lang.IndexOutOfBoundsException e) {}
	}
	public void afficher_questionnaire_a() {
		for (int i = 0 ; i < this.getliste_question().size() ; i++) {
			this.getliste_question().get(i).afficherQuestion_a();
		}
		for (int i = 0 ; i < this.getliste_QCM().size() ; i++) {
			this.getliste_QCM().get(i).afficherQuestion_a();
		}
	}
	
	
	public void afficher_Tentatives_Classe() {
		int i;
		for (i=0; i<this.getListe_rep_c().size(); i++) {
			System.out.println("El�ve : " +this.getListe_rep_c().get(i).getStudent().getNom() +" " +this.getListe_rep_c().get(i).getStudent().getPrenom());
			this.getListe_rep_c().get(i).afficher();
			System.out.println("\n Note :" +this.getListe_rep_c().get(i).Getsomme_A() +"/" +this.getPoints_Questionnaire());
		}
	}
	/*
	public float moyenne_questionnaire() {
		int i;
		float somme;
		somme=0;
		for (i=0; i<this.nb_q; i++) {
			somme = somme + liste_rep_c.getliste_rep_c().get(i).getsomme_note();
		}
		return somme;
	}
	
	public void afficher_moyenne_questionnaire() {
		System.out.println("La moyenne du questionnaire : "+moyenne_questionnaire());
	}
	*/
	
	public boolean existsQuestionnaire(int idQuestionnaire) {
		return this.getId() == idQuestionnaire;
	}
	
	public int existStudent(Apprenant student) {
		boolean trouve = false;
		int rep = NOT_FOUND;
		int i = 0;
		while(!trouve && i < this.getListe_rep_c().size()) {
			if (this.getListe_rep_c().get(i).getStudent().equals(student)) {
				trouve=true;
				rep = i;
			}
			i++;
		}
		return rep;
	}
	
	public boolean same_length_rep(int i) {
		return this.getliste_question().size() == this.getListe_rep_c().get(i).getliste_q().size();
	}
	
	
	// M�thodes
	
	public boolean question_presente(String intitule) {
		int i=0;
		boolean res=false;
		while ((i<this.getliste_question().size())&&(res==false)) {
			if (intitule.equals(getliste_question().get(i).getIntitule())) {
				res=true;
				System.out.println("Question d�j� pos�e");
			}else {
				i++;
			}
		}
		i=0;
		while ((i<this.getliste_QCM().size())&&(res==false)) {
			if (intitule.equals(getliste_QCM().get(i).getIntitule())) {
				res=true;
				System.out.println("Question d�j� pos�e");
			}else {
				i++;
			}
		}
		return res;
	}
	
	// Ajouter et Retirer Question
	
	public void ajouter_Question(Question q) {
		if (question_presente(q.getIntitule())==false) {
			getliste_question().add(q);
		}else {
			System.out.println("Question d�j� pos�e");
		}
	}
	
	public void ajouter_QCM(QCM q) {
		if (question_presente(q.getIntitule())==false) {
			getliste_QCM().add(q);
		}else {
			System.out.println("Question d�j� pos�e");
		}
	}
	
	public void RetirerQuestion(Question q) {
		int i=0;
		boolean res=false;
		while ((i<this.getliste_question().size())&&(res==false)) {
			if (q.getIntitule().equals(getliste_question().get(i).getIntitule())) {
				res=true;
				getliste_question().remove(q);
			}else {
				i++;
			}
		}
		i=0;
		while ((i<this.getliste_QCM().size())&&(res==false)) {
			if (q.getIntitule().equals(getliste_QCM().get(i).getIntitule())) {
				res=true;
				getliste_question().remove(q);
			}else {
				i++;
			}
		}
	}
	
	
	// Ajouter et Retirer ListeTentativeA
	
	public void ajouter_ListeTentativeA(ListeTentativesA a) {
		int i;
		boolean trouve = false;
		if (this.getListe_rep_c().size() == 0) {
			this.liste_rep_c.add(a);
		}
		else {
			i=0;
			while (i < this.getListe_rep_c().size() && trouve != true){
				if(this.getListe_rep_c().get(i).getStudent() == a.getStudent()) {
					trouve = true;
				}
				i=i+1;
			}
			if (trouve != true) {
				this.liste_rep_c.add(a);
			}
		}
	}
	
	public void Retirer_ListeTentativeA(ListeTentativesA a) {
		int i;
		boolean trouve = false;
		if (this.getListe_rep_c().size() == 0) {
			System.out.println("Warning : Aucune Liste");
		}
		else {
			i=0;
			while (i < this.getListe_rep_c().size() && trouve != true){
				if(this.getListe_rep_c().get(i).getStudent() == a.getStudent()) {
					trouve = true;
					this.liste_rep_c.remove(a);
				}
				i=i+1;
			}
			if (trouve == false) {
				System.out.println("Warning : Non trouv�");
			}
		}
	}
	
	
}

