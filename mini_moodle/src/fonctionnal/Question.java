package fonctionnal;
import java.util.ArrayList;

public abstract class Question {
	
	public final int NOT_FOUND = -1;
	
	private String id;
	private float nb_points;
	private String intitule;
	private ArrayList<Proposition> liste_poss;	
	
	// Constructeur de la Super Classe Abstraite Question =======================================================
	
		public Question(String intitule, String id, float pourc) {
			this.nb_points = pourc;
			this.intitule = intitule;
			this.id = id;
			this.liste_poss = new ArrayList<Proposition>();
		}
		
		
		
	// GETTERS & SETTERS =======================================================
	
	// Getters 
		
		public String getId() {
			return this.id;
		}
		
		public float getnb_point() {
			return this.nb_points;
		}
	
		public String getIntitule() {
			return this.intitule;
		}
		
		public ArrayList<Proposition> getListe_poss() {
			return this.liste_poss;
		}
		
	// Setters
		
		public void setId(String id) {
			this.id = id;
		}
		
		public void setnb_point(float nbp) {
			this.nb_points = nbp;
		}
	
		public void setIntitule(String intitule) {
			this.intitule = intitule;
		}
	
		
		
	// M�thodes Abstraites =======================================================
		
	// Get et Affichage de Type
		
		public abstract String getType();
		public abstract void afficherType();

	// Afficher la question et ses possibilit�s de r�ponses version prof et apprenant
		
		public abstract void afficherQuestion_p();
		public abstract void afficherQuestion_a();
		
	// Cr�er une nouvelle proposition
		
		public abstract boolean ajouterProposition(Proposition p);
	
	// Retirer une proposition
	
		public abstract int retirerProposition(Proposition p);
	
	
		
	// M�thodes =======================================================
		
	// Afficher intitul� de la question et ses points
		
		public void afficherIntitule_points() {
			System.out.println(this.intitule +" (" +this.nb_points +" points)");
		}
		
	// Comparaison entre 2 questions
		public boolean equals(Question question) {
			if (this.getId() == question.getId()) return true;
			return false;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((intitule == null) ? 0 : intitule.hashCode());
			return result;
		}
		
	}
