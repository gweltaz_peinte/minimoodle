package fonctionnal;

public class PropositionQCM extends Proposition { //TODO se d�cider ... soit diff�rentier partout QCM soit garder partout l'extends 
													//mais l�, c'est injustifiable (pour l'oral)
	
	private float pourcentage;
	
	// Constructeur de la Sous Classe Proposition =======================================================

	public PropositionQCM(String inti, boolean corr, float pourc) {
		super(inti, corr);
		checkPoint(corr, pourc);		// V�rification Coh�rence Points/Correction
	}

	
	// GETTERS & SETTERS =======================================================
	
	// Getters
	
	public float getPourcentage() {
		return this.pourcentage;
	}
	
	// Setters

	public void setPourcentage(float pourcentage) {
		this.pourcentage = pourcentage;
	}

	
	
	// M�thodes =======================================================
	
	// Afficher Version Professeur intitul� de la Proposition, sa correction et ses points
	
	public void afficher_p() {
		System.out.println(this.getIntitule() + " : " + this.getCorrection() + " (" + this.getPourcentage() + " % )");
	}
	
	// Afficher Version Apprenant intitul� de la Proposition
	
	public void afficher_a() {
		System.out.println(this.getIntitule());
	}
	
	// V�rification Coh�rence Points/Correction
	
	public void checkPoint(boolean corr, float pourc) {
		if (corr == false && pourc>0) {
			System.out.println("Warning : La r�ponse est fausse et elle rapporte des points\n=> La valeur n'a pas �t� affect�e");
		}
		else if (corr == true && pourc <= 0) {
			System.out.println("Warning : La r�ponse est vraie et elle enl�ve des points\n=> La valeur n'a pas �t� affect�e");
		}
		else {
			this.pourcentage = pourc;
		}
	}
}
