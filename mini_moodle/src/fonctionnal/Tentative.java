package fonctionnal;

public abstract class Tentative {
	private Question Ques;
	
	// Constructeur de la Super Classe Abstraite Tentative =======================================================
	
	public Tentative(Question q) {
		this.Ques = q;
	}

	
	
	// GETTERS & SETTERS =======================================================
	
	// Getters
		
	public Question getQuestion() {
		return this.Ques;
	}

	// Setters
	
	public void setQuestion(Question Ques) {
		this.Ques = Ques;
	}
	
	
	
	// M�thodes Abstraites =======================================================
	
	// Get des Points de la Tentative
	
	public abstract float getpoints();
	
	// Get et Affichage de Type
	
	public abstract void type_t();
	
	public String getType() {
		return this.getClass().getName();
	}
	
	public void afficherType() {
		System.out.println(this.getClass().getName());
	}
	
	// Afficher la Tentative
		
	public abstract void afficher();
}
