package fonctionnal;

import java.util.ArrayList;

public class QCM {
		public final int NOT_FOUND = -1;
		
		private String id;
		private float nb_point;
		private String intitule;
		private ArrayList<PropositionQCM> liste_poss;		
		
		// Constructeur de la Classe QCM =======================================================
		
		public QCM(String intitule, String id, float nbp) {
			this.nb_point = nbp;
			this.intitule = intitule;
			this.id = id;
			this.liste_poss = new ArrayList<PropositionQCM>();
		}
		
		
		
	// GETTERS & SETTERS =======================================================
	
	// Getters
		
		public String getId() {
			return this.id;
		}
		
		public float getnb_point() {
			return this.nb_point;
		}
		
		public String getIntitule() {
			return this.intitule;
		}
		
		public ArrayList<PropositionQCM> getListe_poss() {
			return this.liste_poss;
		}
		
		public String getType() {
			return this.getClass().getName();
		}
		
	// Setters
		
		public void setId(String id) {
			this.id = id;
		}
		
		public void setnb_point(float nbp) {
			this.nb_point = nbp;
		}
	
		public void setIntitule(String intitule) {
			this.intitule = intitule;
		}
	
	
	// M�thodes =======================================================
		
	// Afficher intitul� de la question et ses points
		
		public void afficherIntitule_points() {
			System.out.println(this.intitule +" (" +this.nb_point +" points)");
		}

	/* Afficher Question Ouverte Version Professeur
	 * - Affiche Intitule Question
	 * - Affiche Points Question
	 * - Affiche Propositions Question avec la Correction (Vrai/Faux)
	 *  */
	
	public void afficherQuestion_p() {
		afficherIntitule_points();
		for (int i=0; i<this.getListe_poss().size(); i++) {
			this.getListe_poss().get(i).afficher_p();
		}
	}
	
	/* Afficher Question Ouverte Version Apprenant
	 * - Affiche Intitule Question
	 * - Affiche Points Question
	 *  */
	
	public void afficherQuestion_a() {
		afficherIntitule_points();
		int i;
		for (i=0; i<this.getListe_poss().size(); i++) {
			this.getListe_poss().get(i).afficher_a();
		}
	}
	
	// Afficher le type de la question
	
	public void afficherType() {
		System.out.println("Type :" +this.getType());
	}
	
	// Ajouter une nouvelle PropositionQCM 
	
	public boolean ajouterPropositionQCM(PropositionQCM p) {
		int i = 0;
		float somme = 0;														// Somme Pourcentage des propositions
		float pourcen = 0;
		boolean trouve = false;
		if (this.getListe_poss().size() == 0) {									// Cas o� liste vide
			somme = p.getPourcentage();												
			if (somme > 100) {														// V�rification pourcentage
				System.out.println("Warning : pourcentage > 100%");
			}
			else {
				this.getListe_poss().add(p);										// Ajout Proposition
			}
		}
		else {																	// Cas o� liste non vide
				for (i=0; i<this.getListe_poss().size(); i++) {						// Somme des pourcentages
					pourcen = this.getListe_poss().get(i).getPourcentage();
					somme = somme + pourcen;
					if (this.getListe_poss().get(i) == p) {							// V�rification pr�sence
						trouve = true;
					}
				}
				somme = somme + p.getPourcentage();
				
				if (somme > 100) {													// V�rification pourcentage
					System.out.println("Warning : pourcentage > 100%");					
				}
				else if (trouve == false) {
					this.getListe_poss().add(p);									// Ajout Proposition
				}
		}
		return trouve;																// Retourne si Proposition d�j� pr�sente
	}
	
	// Retirer une PropositionQCM 
	
	public int retirerPropositionQCM(PropositionQCM p) {
		int i = 0;
		boolean trouve = false;
		int res = NOT_FOUND;														// Indique la position si on retrouve proposition
		while (i < this.getListe_poss().size() && trouve != true) {					// Cherche tant que proposition pas trouv�e
			if (this.getListe_poss().get(i) == p) {									// Cas Trouv�e
				trouve = true;
				this.getListe_poss().remove(i);											// Retire
			} else {
				i = i+1;
			}
		}
		if (trouve) {
			res = i;
		}
		return res;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((intitule == null) ? 0 : intitule.hashCode());
		return result;
	}

	// Comparaison entre 2 questions
	public boolean equals(Question question) {
		if (this.getId() == question.getId()) return true;
		return false;
	}
}

