package fonctionnal;

public class Tentative_QCU extends Tentative {
	
	private String intitule;
	private float points;
	
	// Constructeur de la Sous Classe Tentative_QCU =======================================================

	public Tentative_QCU(Question q, String inti) {
		super(q);
		this.intitule = inti;
		
		/*On d�finie l'intitul� s'il correspond aux posibilit�es*/
		int i = 0;
		boolean trouve = false;
		while (i<this.getQuestion().getListe_poss().size() && trouve == false) {
			if (inti == this.getQuestion().getListe_poss().get(i).getIntitule()) {
				this.intitule = inti;
				trouve = true;
			}
			else {
				i = i+1;
			}
		}
		/*On d�finie les points par la correction*/
		if (trouve == true) {
			this.points = (this.getQuestion().getListe_poss().get(i).getPourcentage() * this.getQuestion().getnb_point())/100 ;
		}
	}
	

	// GETTERS & SETTERS =======================================================
	
	// Getters
	
	public String getIntitule() {
		return this.intitule;
	}
	
	public float getpoints() {
		return this.points;
	}
	
	public String getType() {
		return this.getClass().getName();
	}

	// Setters
	
	public void setIntitule(String inti) {
		this.intitule = inti;
	}
	
	public void set_points(float points) {
		this.points = points;
	}
	
	// Afficher la r�ponse 

	@Override
	public void afficher(){
		System.out.println(this.getIntitule() +" (" +this.getpoints() + ")");
	}

	// Afficher le Type 
	
	@Override
	public void type_t() {
		System.out.println("Type :" +this.getType());
	}	
}
