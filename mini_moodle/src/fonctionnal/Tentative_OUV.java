package fonctionnal;

public class Tentative_OUV extends Tentative {
	
	private String intitule;
	private float points;

	// Constructeur de la Sous Classe Tentative_OUV =======================================================

	public Tentative_OUV(Question q, String inti) {
		super(q);
		this.intitule = inti;
		
		/*On d�finie les points par la correction*/
		if (inti == this.getQuestion().getListe_poss().get(0).getIntitule()) {
			this.points = this.getQuestion().getnb_point();
		}
		else {
			this.points = 0;
		}
		
	}
	

	// GETTERS & SETTERS =======================================================
	
	// Getters
	
	public String getIntitule() {
		return this.intitule;
	}
	
	public float getpoints() {
		return this.points;
	}
	
	public String getType() {
		return this.getClass().getName();
	}

	// Setters
	
	public void setIntitule(String intitule) {
		this.intitule = intitule;
	}
	
	public void set_points(float points) {
		this.points = points;
	}

	// Afficher la r�ponse 

	@Override
	public void afficher(){
		System.out.println(this.getIntitule() +" (" +this.getpoints() + ")");
	}

	// Afficher le Type 
	
	@Override
	public void type_t() {
		System.out.println("Type :" +this.getType());
	}
}
