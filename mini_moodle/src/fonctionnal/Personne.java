package fonctionnal;

public abstract class Personne {
	private String nom;
	private String prenom;
	private String identifiant;
	private String mot_de_passe;
	
	public Personne (String nom, String prenom, String identifiant, String mot_de_passe) {
		this.nom=nom;
		this.prenom=prenom;
		this.identifiant=identifiant;
		this.mot_de_passe=mot_de_passe;
	}
	
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	public String getIdentifiant() {
		return identifiant;
	}
	public void setIdentifiant(String identifiant) {
		this.identifiant = identifiant;
	}
	public String getMot_de_passe() {
		return mot_de_passe;
	}
	public void setMot_de_passe(String mot_de_passe) {
		this.mot_de_passe = mot_de_passe;
	}

	public abstract String toString();
	
	public void affiche() {
		System.out.println(this);
	}
	public void setEtatCivil(String nom,String prenom) {
		this.setNom(nom);
		this.setPrenom(prenom);
	}
	public void setIdentifants(String identifiant, String mot_de_passe) {
		this.setIdentifiant(identifiant);
		this.setMot_de_passe(mot_de_passe);
	}
	public boolean isLoginEqual(String identifiant) {
		boolean res = false;
		if (identifiant != null && this.getIdentifiant() ==identifiant) {
			res = true;
		}
		return res;
	}
	public boolean isPwdEqual(String mot_de_passe) {
		boolean res = false;
		if (mot_de_passe != null && this.getMot_de_passe() == mot_de_passe) {
			res = true;
		}
		return res;
	}

}
