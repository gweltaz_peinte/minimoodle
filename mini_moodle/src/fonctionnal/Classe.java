package fonctionnal;

import java.util.ArrayList;

public class Classe {
	private int identifiant;
	private ArrayList<Apprenant> apprenants;
	private Professeur enseignant;
	private ArrayList<Questionnaire> questionnaires;
	
	private static int nbClasse = 0;
	
	public Classe (Professeur enseignant) {
		this.apprenants = new ArrayList<Apprenant>();
		this.questionnaires = new ArrayList<Questionnaire>();
		this.enseignant = enseignant;
		this.identifiant = Classe.nbClasse;
		Classe.nbClasse = Classe.nbClasse + 1;
	}
	
	public int getIdentifiant() {
		return this.identifiant;
	}

	public void setIdentifiant(int identifiant) {
		this.identifiant = identifiant;
	}

	public ArrayList<Apprenant> getApprenants() {
		return this.apprenants;
	}

	public Professeur getEnseignant() {
		return this.enseignant;
	}

	public void setEnseignant(Professeur enseignant) {
		this.enseignant = enseignant;
	}
	public void removeEnseignant() {
		this.enseignant = null;
	}

	public ArrayList<Questionnaire> getQuestionnaires() {
		return this.questionnaires;
	}
	
	public boolean isStudentPresent(Apprenant student) {
		boolean res = false;
		int i = 0;
		while (!res && i < this.apprenants.size()) {
			if (this.apprenants.get(i).equals(student)) {
				res=true;
			}
			i++;
		}
		return res;
	}
	
	public void addStudent(Apprenant student) {
		if (!this.isStudentPresent(student)) {
			this.apprenants.add(student);
		}
	}
	public void removeStudent(Apprenant student) {
		if (this.isStudentPresent(student)) {
			this.apprenants.remove(student);
		}
	}

	public void removeStudents() {
		for(int i = this.getApprenants().size() ; i > 0  ; i--) {
			this.getApprenants().remove(i);
		}
	}
	
	public static int getNbClasse() {
		return Classe.nbClasse;
	}

	public static void setNbClasse(int nbClasse) {
		Classe.nbClasse = nbClasse;
	}
	
	public void removeQuestionnaires() {
		for(int i = this.getQuestionnaires().size() ; i > 0  ; i--) {
			this.getQuestionnaires().remove(i);
		}
	}
	
	public ArrayList<Questionnaire> getQuestionnairesFaits(Apprenant student){
		ArrayList<Questionnaire> res = new ArrayList<Questionnaire>();
		for (int i=0; i < this.getQuestionnaires().size() ;i++) {
			int exists = this.getQuestionnaires().get(i).existStudent(student);
			if (exists != -1) {
				if (this.getQuestionnaires().get(i).same_length_rep(exists)) {
					res.add(this.getQuestionnaires().get(i));
				}
			}
		}
		return res;
	}
	
	public ArrayList<Questionnaire> getQuestionnairesAFaire(Apprenant student){
		ArrayList<Questionnaire> res = new ArrayList<Questionnaire>();
		for (int i=0; i < this.getQuestionnaires().size() ;i++) {
			int exists = this.getQuestionnaires().get(i).existStudent(student);
			if(exists >= 0) {
				if (!this.getQuestionnaires().get(i).same_length_rep(exists)) {
					res.add(this.getQuestionnaires().get(i));
				}
			} else {
				res.add(this.getQuestionnaires().get(i));
			}
		}
		return res;
	}
	
	
	
	@Override
	public String toString() {
		String res = "Classe" + identifiant + " (" + enseignant.getMatiere_enseignee() + " avec "
	+ enseignant.getPrenom() + " " + enseignant.getNom() + ") : \n" + "apprenants = [";
		if (this.getApprenants().size() > 0) {
			res += "\n";
		}
		for (int i = 0 ; i < apprenants.size() ; i++) {
			res += "\t"+ apprenants.get(i).getPrenom() + " " + apprenants.get(i).getNom() + "\n";
		}
		res += "]";
		return res;
	}
	
	public void createQuestionnaire(Professeur enseignant) {
		if(this.getEnseignant().equals(enseignant)) {
			Questionnaire questionnaire1 = new Questionnaire(enseignant);
			this.getQuestionnaires().add(questionnaire1);
		}
	}
	
	public void createQuestion(Professeur enseignant, String idQuestionnaire) {
		/*
		 * vérifier que la personne qui demande l'opération /\ est un professeur
		 * creer la question 
		 * ajouter la question au questionnaire (lequel ? /\ ; il existe ?)
		 */
		if(this.getEnseignant().equals(enseignant)) {
			//Question question1 = new Question();
			//TODO
		}
	}
}
