package fonctionnal;

import java.io.Serializable;

public class DATA implements Serializable {

	private Questionnaire questionnaire;
	private ListeTentativesA tentative;
	private Classe classe;
	private Apprenant apprenant;
	private Professeur professeur;
	
	public DATA(ListeTentativesA tentative, Classe classe, Questionnaire questionnaire, Apprenant apprenant, Professeur professeur) {
		
		this.questionnaire = questionnaire;
		this.tentative = tentative;
		this.classe = classe;
		this.apprenant = apprenant;
		this.professeur = professeur;
				
	}
	
	public Classe getClasse() {
		return classe;
	}

	public void setClasse(Classe classe) {
		this.classe = classe;
	}
	
	public ListeTentativesA getTentative() {
		return tentative;
	}
	public void setTentative(ListeTentativesA tentative) {
		this.tentative = tentative;
	}

	public Questionnaire getQuestionnaire() {
		return questionnaire;
	}

	public void setQuestionnaire(Questionnaire questionnaire) {
		this.questionnaire = questionnaire;
	}

	public Apprenant getApprenant() {
		return apprenant;
	}

	public void setApprenant(Apprenant apprenant) {
		this.apprenant = apprenant;
	}

	public Professeur getProfesseur() {
		return professeur;
	}

	public void setProfesseur(Professeur professeur) {
		this.professeur = professeur;
	}
}
