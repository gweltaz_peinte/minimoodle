package fonctionnal;
import java.util.ArrayList;

public class ListeTentativeQCM {
	
	public final int NOT_FOUND = -1;
	
	private ArrayList<Tentative_QCM> liste_r;
	private Apprenant student;
	private QCM ques;
	
	// Constructeur de la Classe ListeTentativeQCM =======================================================
	
	public ListeTentativeQCM(Apprenant student, QCM q) {
		liste_r = new ArrayList<Tentative_QCM>();
		this.student = student;
		this.ques = q;
	}
	
	// GETTERS & SETTERS =======================================================
	
	// Getters 
	
	public ArrayList<Tentative_QCM> getliste_r() {
		return liste_r;
	}
	
	public Apprenant getStudent() {
		return this.student;
	}
	
	public QCM getQuestion() {
		return this.ques;
	}
	
	// Setters 

	public void setliste_q(ArrayList<Tentative_QCM> liste_r) {
		this.liste_r = liste_r;
	}
	
	public void setStudent(Apprenant student) {
		this.student = student;
	}
	
	public void setQuestion(QCM q) {
		this.ques = q;
	}
	
	
	// M�thodes Abstraites =======================================================
	
	// Get Somme Note Liste Tentatives
	
	public float Getsomme_note_lr() {
		int i;
		float somme = 0;
		for (i=0; i < this.liste_r.size(); i++) {
			somme = somme + this.liste_r.get(i).getpoints();
		}
		if (somme < 0) {
			somme = 0;
		}
		return somme;
	}
	
	// Afficher Liste Tentative
	
	public void afficher() {
		int i;
		for (i=0; i<liste_r.size(); i++) {											// Affiche toutes les tentatives
			this.liste_r.get(i).afficher();;
		}
		System.out.println("Somme : " +this.Getsomme_note_lr() +"\n");				// Affiche somme notes
	}
	
	// Ajouter une Tentative
	
	public void ajouterTent(Tentative_QCM t) {
		int i = 0;
		boolean trouve = false;
		while(i<this.getliste_r().size() && trouve!=true) {							// Cherche si Tentative_QCM d�j� pr�sente
			if (this.getliste_r().get(i) == t) {
				trouve = true;
			}
			else {
				i = i+1;
			}
		}
		if (trouve == false) {
			this.liste_r.add(t);													// Pas trouve alors ajout
		}
	}
	
	// Retirer une Tentative
	
	public int retirerTentative(Tentative_QCM t) {
		boolean trouve = false;
		int res = NOT_FOUND;														// Indique la position si on retrouve Tentative_QCM
		if (this.getliste_r().get(0) == t) {										// Cherche pr�sence Tentative_QCM
			trouve = true;
			this.getliste_r().remove(0);											// Retire Tentative_QCM
		}	
		if (trouve) {
			res = 0;
		}
		return res;
	}
}

