package fonctionnal;

public class Tentative_QCM {
	
	private QCM question;
	private String intitule;
	private float points;

	// Constructeur de la Sous Classe Tentative_QCM =======================================================
	
	public Tentative_QCM(QCM q, String inti) {
		this.question = q;
		
		/*On d�finie l'intitul� s'il correspond aux posibilit�es*/
		int i = 0;
		boolean trouve = false;
		while (i < this.getQuestion().getListe_poss().size()  && trouve == false) {
			if (inti == this.getQuestion().getListe_poss().get(i).getIntitule()) {
				this.intitule = inti;
				trouve = true;
			}
			else {
				i = i+1;
			}
		}
		
		/*On d�finie les points par la correction*/
		if (trouve == true) {
			this.points = (this.getQuestion().getListe_poss().get(i).getPourcentage() * this.getQuestion().getnb_point())/100 ;
		}
	}
	

	// GETTERS & SETTERS =======================================================
	
	// Getters
	
	private QCM getQuestion() {
		return this.question;
	}
	
	public String getIntitule() {
		return this.intitule;
	}

	public float getpoints() {
		return this.points;
	}
	
	public String getType() {
		return this.getClass().getName();
	}

	// Setters
	
	public void setQuestion(QCM q) {
		this.question = q;
	}
	
	public void setIntitule(String intitule) {
		this.intitule = intitule;
	}
	
	public void set_points(float points) {
		this.points = points;
	}

	// Afficher la Tentative_QCM 

	public void afficher(){
		System.out.println(this.getIntitule() +" (" +this.getpoints() + ")");
	}

	// Afficher le Type 
	
	public void type_t() {
		System.out.println("Type :" +this.getType());
	}
	
	
}
