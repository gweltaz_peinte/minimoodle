package fonctionnal;

public class Numerique extends Question {

	// Constructeur de la Sous Classe Numerique =======================================================
	
	public Numerique(String intitule, String id, float nbp) {
		super(intitule, id, nbp); // H�ritage de Question
	}
	
	
	// M�thodes Abstraites =======================================================
	
	// Get et Affichage de Type	
	
	public String getType() {
		return this.getClass().getName();
	}
	
	public void afficherType() {
		System.out.println("Type :" +this.getType());
	}

	
	// M�thodes =======================================================
	
	/* Afficher Question Ouverte Version Professeur
	 * - Affiche Intitule Question
	 * - Affiche Points Question
	 * - Affiche Propositions Question avec la Correction (Vrai/Faux)
	 *  */
		
	public void afficherQuestion_p() {
		afficherIntitule_points();
		try{											// Try : Evite erreur quand abscence proposition
			this.getListe_poss().get(0).afficher_p();
		}
		catch (java.lang.IndexOutOfBoundsException e) {}
	}
	
	/* Afficher Question Ouverte Version Apprenant
	 * - Affiche Intitule Question
	 * - Affiche Points Question
	 *  */
	
	public void afficherQuestion_a() {
		afficherIntitule_points();
	}

	// Ajouter une nouvelle proposition 
	
	public boolean ajouterProposition(Proposition p) {
		boolean trouve = true;												// Pr�sence Proposition
		if (this.getListe_poss().size() == 0) {								// Cas Liste Vide
			this.getListe_poss().add(p);										// Ajout Automatique
		}
		else {																// Cas Liste Non vide
			System.out.println("Warning : Correction d�j� donn�e");				// Affichage Warning
		}
		
		return trouve;
	}
	
	// Retirer une proposition 
			
	@Override
	public int retirerProposition(Proposition p) {
		boolean trouve = false;
		int res = NOT_FOUND;											// Indique la position si on retrouve Proposition
		if (this.getListe_poss().get(0) == p) {							// Cherche pr�sence Proposition
			trouve = true;
			this.getListe_poss().remove(0);								// Retire Proposition
		}
		if (trouve) {
			res = 0;
		}
		return res;
	}
	
	
}
