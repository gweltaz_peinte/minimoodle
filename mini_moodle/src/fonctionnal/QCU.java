package fonctionnal;

public class QCU extends Question {
	
	// Constructeur de la Sous Classe QCU =======================================================
	
	public QCU(String intitule, String id, float pourc) {
		super(intitule, id, pourc);
	}
		
	
	// M�thodes Abstraites =======================================================
	
	// Get et Affichage de Type
	
	public String getType() {
		return this.getClass().getName();
	}
	
	public void afficherType() {
		System.out.println("Type :" +this.getType());
	}
	
	
	// M�thodes =======================================================
	
	/* Afficher Question Ouverte Version Professeur
	 * - Affiche Intitule Question
	 * - Affiche Points Question
	 * - Affiche Propositions Question avec la Correction (Vrai/Faux)
	 *  */
	
	public void afficherQuestion_p() {
		afficherIntitule_points();
		for (int i=0; i<this.getListe_poss().size(); i++) {
			this.getListe_poss().get(i).afficher_p();
		}
	}

	/* Afficher Question Ouverte Version Apprenant
	 * - Affiche Intitule Question
	 * - Affiche Points Question
	 *  */
	
	public void afficherQuestion_a() {
		afficherIntitule_points();
		int i;
		for (i=0; i<this.getListe_poss().size(); i++) {
			this.getListe_poss().get(i).afficher_a();
		}
	}

	// Ajouter une nouvelle proposition
	
	public boolean ajouterProposition(Proposition p) {
		int i = 0;
		boolean trouve = false;																	// Indique s'il existe d�j� cette solution
		boolean unique = false;																	// Indique s'il existe d�j� une solution vrai
		if (p.getCorrection() == true) {
			while (i < this.getListe_poss().size() && unique != true && trouve != true) {		// Arret si solution d�j� pr�sente ou s'il y en a une d�j� vraie
				if (this.getListe_poss().get(i) == p) {
					trouve = true;
				}
				else if (this.getListe_poss().get(i).getCorrection() == true) {
					unique = true;
					System.out.println("Warning : Il existe d�j� une proposition vraie");
				}
				i = i+1;
			}
		}
		else {																					// Abscence V�rification s'il existe d�j� une solution vrai 
			while (i < this.getListe_poss().size() && trouve != true) {
				if (this.getListe_poss().get(i) == p) {
					trouve = true;
				}
				i = i+1;
			}
		}
		if (!trouve && !unique) {																// Ajout aucune solution d�j� pr�sente et aucune d�j� vraie
			this.getListe_poss().add(p);
		}
		
		return trouve;
	}
	
	// Retirer une proposition 
	
	@Override
	public int retirerProposition(Proposition p) {
		int i = 0;
		boolean trouve = false;
		int res = NOT_FOUND;																	// Indique la position si on retrouve proposition
		while (i < this.getListe_poss().size() && trouve != true) {								// Cherche tant que proposition pas trouv�e
			if (this.getListe_poss().get(i) == p) {
				trouve = true;
				this.getListe_poss().remove(i);													// Enl�ve si trouv�e
			} else {
				i = i+1;
			}
		}
		if (trouve) {																			// Retourne placement si trouv�e
			res = i;
		}
		return res;
	}
}
