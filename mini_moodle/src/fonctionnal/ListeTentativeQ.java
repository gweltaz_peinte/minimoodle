package fonctionnal;
import java.util.ArrayList;

public class ListeTentativeQ {
	
	public final int NOT_FOUND = -1;
	
	private ArrayList<Tentative> liste_r;
	private Apprenant student;
	private Question ques;

	// Constructeur de la ListeTentativeQ =======================================================

	public ListeTentativeQ(Apprenant student, Question q) {
		liste_r = new ArrayList<Tentative>();
		this.student = student;
		this.ques = q;
	}
	
	// GETTERS & SETTERS =======================================================
	
	// Getters 

	public ArrayList<Tentative> getliste_r() {
		return liste_r;
	}
	
	public Apprenant getStudent() {
		return this.student;
	}
	
	public Question getQuestion() {
		return this.ques;
	}
	
	// Setters 

	public void setliste_q(ArrayList<Tentative> liste_r) {
		this.liste_r = liste_r;
	}
	
	public void setStudent(Apprenant student) {
		this.student = student;
	}
	
	public void setQuestion(Question q) {
		this.ques = q;
	}
	

	
	// M�thodes Abstraites =======================================================

	// Get Somme Note Liste Tentatives
	
	public float Getsomme_note_lr() {
		return this.getliste_r().get(0).getpoints();
	}
	
	// Afficher Liste Tentative
	
	public void afficher() {
		int i;
		for (i=0; i<liste_r.size(); i++) {																		// Affiche toutes les tentatives
			this.liste_r.get(i).afficher();;
		}
		System.out.println("Somme : " +this.Getsomme_note_lr() +"\n");											// Affiche somme notes
	}
	
	// Ajouter une Tentative
	
	public void ajouterTent(Tentative t) {
		if (this.getliste_r().size() > 0) {																		// Cas Tentative d�j� donn�e
			System.out.println("Warning : Une seule tentative possible");											// Affichage Warning
		}
		else {																									// Cas Tentative non donn�e
			this.liste_r.add(t);																					// Ajout Tentative
		}
	}
	
	// Retirer une Tentative
	
	public int retirerTentative(Tentative t) {
		boolean trouve = false;
		int res = NOT_FOUND;																				// Indique la position si on retrouve Tentative_QCM
		if (this.getliste_r().get(0) == t) {																// Cherche pr�sence Tentative_QCM
			trouve = true;
			this.getliste_r().remove(0);																	// Retire Tentative_QCM
		}	
		if (trouve) {
			res = 0;
		}
		return res;
	}
}

