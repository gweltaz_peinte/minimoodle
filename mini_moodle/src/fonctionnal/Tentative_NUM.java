package fonctionnal;

public class Tentative_NUM extends Tentative {
	
	private String intitule;
	private float points;
	
	// Constructeur de la Sous Classe Tentative_NUM =======================================================

	public Tentative_NUM(Question q, String inti) {
		super(q);
		this.intitule = inti;
		
		/*On d�finie les points par la correction*/
		if (inti == this.getQuestion().getListe_poss().get(0).getIntitule()) {		// Cas Tentative est la bonne
			this.points = this.getQuestion().getnb_point();								// Affecte les points
		}
		else {																		// Cas Tentative est la mauvaise
			this.points = 0;															// Affecte 0 point
		}
	}
	
	
	
	// GETTERS & SETTERS =======================================================
	
	// Getters
	
	public String getIntitule() {
		return this.intitule;
	}
	
	public float getpoints() {
		return this.points;
	}
	
	public String getType() {
		return this.getClass().getName();
	}

	// Setters
	
	public void setIntitule(String intitule) {
		this.intitule = intitule;
	}
	
	public void set_points(float points) {
		this.points = points;
	}
	
	
		
	// Afficher la r�ponse 

	@Override
	public void afficher(){
		System.out.println(this.getIntitule() +" (" +this.getpoints() + ")");
	}

	// Afficher le Type 
	
	@Override
	public void type_t() {
		System.out.println("Type :" +this.getType());
	}
}
