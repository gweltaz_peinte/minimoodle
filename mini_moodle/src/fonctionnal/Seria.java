package fonctionnal;

import java.io.EOFException;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

public class Seria implements Serializable{
    private static final long serialVersionUID = 1L;
    
    public static void ecrire(DATA data, String file) throws FileNotFoundException, IOException{
        ObjectOutputStream lienFic = new ObjectOutputStream(new FileOutputStream(file, false));
        lienFic.writeObject(data);
        lienFic.close();
    }
    
    public static DATA lire(String file) throws IOException,ClassNotFoundException{
        ObjectInputStream lienFic = new ObjectInputStream(new FileInputStream(file));
        DATA data = null;
        try {
            while (true) {
                data = (DATA)lienFic.readObject();
            }
        } catch(EOFException e) {
            lienFic.close();
        }
        return data;
    }
    
}